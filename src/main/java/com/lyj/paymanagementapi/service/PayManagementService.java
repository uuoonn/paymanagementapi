package com.lyj.paymanagementapi.service;

import com.lyj.paymanagementapi.entity.PayManagement;
import com.lyj.paymanagementapi.model.InfoChangeRequest;
import com.lyj.paymanagementapi.model.PayManagementItem;
import com.lyj.paymanagementapi.model.PayManagementRequest;
import com.lyj.paymanagementapi.model.PayManagementResponse;
import com.lyj.paymanagementapi.repository.PayManagementRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class PayManagementService {
    private final PayManagementRepository payManagementRepository;

    public void setPayManagement(PayManagementRequest request){
        PayManagement addData = new PayManagement();
        addData.setName(request.getName());
        addData.setBasicPay(request.getBasicPay());
        addData.setNationalPension(request.getNationalPension());
        addData.setEmploymentIns(request.getEmploymentIns());
        addData.setHealthIns(request.getHealthIns());
        addData.setLongCareIns(request.getLongCareIns());
        addData.setIncomeTax(request.getIncomeTax());
        addData.setTaxFree(request.getTaxFree());

        payManagementRepository.save(addData);

    }

    public List<PayManagementItem> getPayManagements() {
        List<PayManagement> originData = payManagementRepository.findAll();

        List<PayManagementItem> result = new LinkedList<>();

        for(PayManagement payManagement : originData){
            PayManagementItem addItem = new PayManagementItem();
            addItem.setId(payManagement.getId());
            addItem.setName(payManagement.getName());
            addItem.setBasicPay(payManagement.getBasicPay());

            result.add(addItem);
        }

        return result;


    }

    public PayManagementResponse getPayManagement(long id){
        PayManagement originData = payManagementRepository.findById(id).orElseThrow();

        PayManagementResponse response = new PayManagementResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setBasicPay(originData.getBasicPay());
        response.setNationalPension(originData.getNationalPension());
        response.setEmploymentIns(originData.getEmploymentIns());
        response.setHealthIns(originData.getHealthIns());
        response.setLongCareIns(originData.getLongCareIns());
        response.setIncomeTax(originData.getIncomeTax());
        response.setTaxFree(originData.getTaxFree());

        return response;

    }

    public void putInfoChange(long id, InfoChangeRequest request){
        PayManagement originData = payManagementRepository.findById(id).orElseThrow();

        originData.setName(request.getName());
        originData.setBasicPay(request.getBasicPay());

        payManagementRepository.save(originData);

    }

    public void delManagement(long id){
        payManagementRepository.deleteById(id);
    }


}
