package com.lyj.paymanagementapi.repository;

import com.lyj.paymanagementapi.entity.PayManagement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PayManagementRepository extends JpaRepository<PayManagement, Long> {
}
