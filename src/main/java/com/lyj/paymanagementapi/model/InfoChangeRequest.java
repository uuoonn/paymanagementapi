package com.lyj.paymanagementapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class InfoChangeRequest {
    private String name;
    private Double basicPay;
}
