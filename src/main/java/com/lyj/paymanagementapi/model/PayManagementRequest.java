package com.lyj.paymanagementapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class PayManagementRequest {

    private String name;
    private Double basicPay;
    private Double nationalPension;
    private Double employmentIns;
    private Double healthIns;
    private Double longCareIns;
    private Double incomeTax;
    private Double taxFree;

}
