package com.lyj.paymanagementapi.controller;

import com.lyj.paymanagementapi.model.InfoChangeRequest;
import com.lyj.paymanagementapi.model.PayManagementItem;
import com.lyj.paymanagementapi.model.PayManagementRequest;
import com.lyj.paymanagementapi.model.PayManagementResponse;
import com.lyj.paymanagementapi.repository.PayManagementRepository;
import com.lyj.paymanagementapi.service.PayManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pay")

public class PayManagementController {
    private final PayManagementService payManagementService;
    @PostMapping("/new")
    public String setPayManagement(@RequestBody PayManagementRequest request){
        payManagementService.setPayManagement(request);

        return "ok";
    }

    @GetMapping("/list")
    public List<PayManagementItem> getPayManagements(){
        return payManagementService.getPayManagements();
    }

    @GetMapping("/detail/{id}")
    public PayManagementResponse getPayManagement(@PathVariable long id){
        return payManagementService.getPayManagement(id);
    }

    @PutMapping("/edit/{id}")
    public String putInfoChange(@PathVariable long id , @RequestBody InfoChangeRequest request ){
        payManagementService.putInfoChange(id,request);

        return "ok";

    }

    @DeleteMapping("/{id}")
    public String delManagement(@PathVariable long id){
        payManagementService.delManagement(id);

        return "ok";
    }
}
